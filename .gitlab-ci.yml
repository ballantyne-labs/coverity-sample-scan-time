stages:
  - build
  - full-scan
  - incremental-scan
  - commit

variables:
  MAVEN_CLI_OPTS: "-DskipTests -Dmaven.test.skip=true"
  COV_ANALYSIS_FLAGS: "--all --webapp-security"
  COV_STREAM: "WebGoat-sample-scan-time"

cache:
  paths:
    - .m2/repository
  key: 'myCache'

build:
  image: maven
  stage: build
  script:
    - 'mvn $MAVEN_CLI_OPTS clean compile'
  when:
    manual


cov-build+fs-full: 
  image: registry.gitlab.com/ballantyne-labs/coverity-docker-images/cov_analysis:$COV_VERSION
  stage: full-scan
  cache:
    key: cov-build+fs
    paths:
      - idir/
  script:
    - rm -rf idir
    - wget $COV_LICENSE_PATH/license.dat -O /opt/coverity/analysis/bin/license.dat &>/dev/null
    - time cov-build --dir idir --fs-capture-search . mvn $MAVEN_CLI_OPTS clean compile
    - time cov-analyze --dir idir --strip-path "$(pwd)" $COV_ANALYSIS_FLAGS

cov-build+fs-incremental: 
  image: registry.gitlab.com/ballantyne-labs/coverity-docker-images/cov_analysis:$COV_VERSION
  stage: incremental-scan
  needs: ["cov-build+fs-full"]
  cache:
    key: cov-build+fs
    paths:
      - idir/
  script:
    - wget $COV_LICENSE_PATH/license.dat -O /opt/coverity/analysis/bin/license.dat &>/dev/null
    - time cov-manage-emit --dir idir reset-host-name
    - time cov-build --dir idir --fs-capture-search . mvn $MAVEN_CLI_OPTS clean compile
    - time cov-analyze --dir idir --strip-path "$(pwd)" $COV_ANALYSIS_FLAGS

cov-build+fs-commit: 
  image: registry.gitlab.com/ballantyne-labs/coverity-docker-images/cov_analysis:$COV_VERSION
  stage: commit
  needs: ["cov-build+fs-incremental"]
  cache:
    key: cov-build+fs
    paths:
      - idir/
  script:
    - wget $COV_LICENSE_PATH/license.dat -O /opt/coverity/analysis/bin/license.dat &>/dev/null
    - time cov-manage-emit --dir idir reset-host-name
    - 'time cov-commit-defects --dir idir --url $COV_URL --stream $COV_STREAM --version $COV_VERSION --description $(echo $CI_JOB_NAME | rev | cut -d- -f2-  | rev)'
  allow_failure: true

cov-build+cov-capture-source-dir-full: 
  image: registry.gitlab.com/ballantyne-labs/coverity-docker-images/cov_analysis:$COV_VERSION
  stage: full-scan
  cache:
    key: cov-build+cov-capture-source-dir
    paths:
      - idir/
  script:
    - rm -rf idir
    - wget $COV_LICENSE_PATH/license.dat -O /opt/coverity/analysis/bin/license.dat &>/dev/null
    - 'time cov-build --dir idir mvn $MAVEN_CLI_OPTS clean compile'
    - 'time cov-capture --dir idir --source-dir .'
    - time cov-analyze --dir idir --strip-path "$(pwd)" $COV_ANALYSIS_FLAGS

cov-build+cov-capture-source-dir-incremental: 
  image: registry.gitlab.com/ballantyne-labs/coverity-docker-images/cov_analysis:$COV_VERSION
  stage: incremental-scan
  needs: ["cov-build+cov-capture-source-dir-full"]
  cache:
    key: cov-build+cov-capture-source-dir
    paths:
      - idir/
  script:
    - wget $COV_LICENSE_PATH/license.dat -O /opt/coverity/analysis/bin/license.dat &>/dev/null
    - time cov-manage-emit --dir idir reset-host-name
    - 'time cov-build --dir idir mvn $MAVEN_CLI_OPTS clean compile'
    - 'time cov-capture --dir idir --source-dir .'
    - time cov-analyze --dir idir --strip-path "$(pwd)" $COV_ANALYSIS_FLAGS

cov-build+cov-capture-source-dir-commit: 
  image: registry.gitlab.com/ballantyne-labs/coverity-docker-images/cov_analysis:$COV_VERSION
  stage: commit
  needs: ["cov-build+cov-capture-source-dir-incremental"]
  cache:
    key: cov-build+cov-capture-source-dir
    paths:
      - idir/
  script:
    - wget $COV_LICENSE_PATH/license.dat -O /opt/coverity/analysis/bin/license.dat &>/dev/null
    - time cov-manage-emit --dir idir reset-host-name
    - 'time cov-commit-defects --dir idir --url $COV_URL --stream $COV_STREAM --version $COV_VERSION --description $(echo $CI_JOB_NAME | rev | cut -d- -f2-  | rev)'
  allow_failure: true


coverity-cli-full: 
  image: registry.gitlab.com/ballantyne-labs/coverity-docker-images/cov_analysis:$COV_VERSION
  stage: full-scan
  cache:
    key: coverity-cli
    paths:
      - idir/
  script:
    - rm -rf idir
    - wget $COV_LICENSE_PATH/license.dat -O /opt/coverity/analysis/bin/license.dat &>/dev/null
    - time coverity capture
    - time cov-analyze --dir idir --strip-path "$(pwd)" $COV_ANALYSIS_FLAGS

coverity-cli-incremental: 
  image: registry.gitlab.com/ballantyne-labs/coverity-docker-images/cov_analysis:$COV_VERSION
  stage: incremental-scan
  needs: ["coverity-cli-full"]
  cache:
    key: coverity-cli
    paths:
      - idir/
  script:
    - wget $COV_LICENSE_PATH/license.dat -O /opt/coverity/analysis/bin/license.dat &>/dev/null
    - time cov-manage-emit --dir idir reset-host-name
    - time coverity capture
    - time cov-analyze --dir idir --strip-path "$(pwd)" $COV_ANALYSIS_FLAGS

coverity-cli-commit: 
  image: registry.gitlab.com/ballantyne-labs/coverity-docker-images/cov_analysis:$COV_VERSION
  stage: commit
  needs: ["coverity-cli-incremental"]
  cache:
    key: coverity-cli
    paths:
      - idir/
  script:
    - wget $COV_LICENSE_PATH/license.dat -O /opt/coverity/analysis/bin/license.dat &>/dev/null
    - mkdir --parents ~/.coverity/authkeys
    - wget $COV_LICENSE_PATH/$COV_AUTHKEY_NAME -O ~/.coverity/authkeys/$COV_AUTHKEY_NAME  &>/dev/null
    - time cov-manage-emit --dir idir reset-host-name
    - 'time cov-commit-defects --dir idir --url $COV_URL --stream $COV_STREAM --version $COV_VERSION --description $(echo $CI_JOB_NAME | rev | cut -d- -f2-  | rev)'
  allow_failure: true

cov-capture-project-dir-full: 
  image: registry.gitlab.com/ballantyne-labs/coverity-docker-images/cov_analysis:$COV_VERSION
  stage: full-scan
  cache:
    key: cov-capture-project-dir
    paths:
      - idir/
  script:
    - rm -rf idir
    - wget $COV_LICENSE_PATH/license.dat -O /opt/coverity/analysis/bin/license.dat &>/dev/null
    - time cov-capture --dir idir --project-dir .
    - time cov-analyze --dir idir --strip-path "$(pwd)" $COV_ANALYSIS_FLAGS

cov-capture-project-dir-incremental: 
  image: registry.gitlab.com/ballantyne-labs/coverity-docker-images/cov_analysis:$COV_VERSION
  stage: incremental-scan
  needs: ["cov-capture-project-dir-full"]
  cache:
    key: cov-capture-project-dir
    paths:
      - idir/
  script:
    - wget $COV_LICENSE_PATH/license.dat -O /opt/coverity/analysis/bin/license.dat &>/dev/null
    - time cov-manage-emit --dir idir reset-host-name
    - time cov-capture --dir idir --project-dir .
    - time cov-analyze --dir idir --strip-path "$(pwd)" $COV_ANALYSIS_FLAGS

cov-capture-project-dir-commit: 
  image: registry.gitlab.com/ballantyne-labs/coverity-docker-images/cov_analysis:$COV_VERSION
  stage: commit
  needs: ["cov-capture-project-dir-incremental"]
  cache:
    key: cov-capture-project-dir
    paths:
      - idir/
  script:
    - wget $COV_LICENSE_PATH/license.dat -O /opt/coverity/analysis/bin/license.dat &>/dev/null
    - time cov-manage-emit --dir idir reset-host-name
    - 'time cov-commit-defects --dir idir --url $COV_URL --stream $COV_STREAM --version $COV_VERSION --description $(echo $CI_JOB_NAME | rev | cut -d- -f2-  | rev)'
  allow_failure: true

cov-capture-source-dir-full: 
  image: registry.gitlab.com/ballantyne-labs/coverity-docker-images/cov_analysis:$COV_VERSION
  stage: full-scan
  cache:
    key: cov-capture-source-dir
    paths:
      - idir/
  script:
    - rm -rf idir
    - wget $COV_LICENSE_PATH/license.dat -O /opt/coverity/analysis/bin/license.dat &>/dev/null
    - time cov-capture --dir idir --source-dir .
    - time cov-analyze --dir idir --strip-path "$(pwd)" $COV_ANALYSIS_FLAGS

cov-capture-source-dir-incremental: 
  image: registry.gitlab.com/ballantyne-labs/coverity-docker-images/cov_analysis:$COV_VERSION
  stage: incremental-scan
  needs: ["cov-capture-source-dir-full"]
  cache:
    key: cov-capture-source-dir
    paths:
      - idir/
  script:
    - wget $COV_LICENSE_PATH/license.dat -O /opt/coverity/analysis/bin/license.dat &>/dev/null
    - time cov-manage-emit --dir idir reset-host-name
    - time cov-capture --dir idir --source-dir .
    - time cov-analyze --dir idir --strip-path "$(pwd)" $COV_ANALYSIS_FLAGS

cov-capture-source-dir-commit: 
  image: registry.gitlab.com/ballantyne-labs/coverity-docker-images/cov_analysis:$COV_VERSION
  stage: commit
  needs: ["cov-capture-source-dir-incremental"]
  cache:
    key: cov-capture-source-dir
    paths:
      - idir/
  script:
    - wget $COV_LICENSE_PATH/license.dat -O /opt/coverity/analysis/bin/license.dat &>/dev/null
    - time cov-manage-emit --dir idir reset-host-name
    - 'time cov-commit-defects --dir idir --url $COV_URL --stream $COV_STREAM --version $COV_VERSION --description $(echo $CI_JOB_NAME | rev | cut -d- -f2-  | rev)'
  allow_failure: true
